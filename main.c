//
//  main.c 
//  N-Body Simulation
//
//  Created by Kseniia Priakhina on 12/24/17.
//  Copyright © 2017 AUCA. All rights reserved.
//

#include <stdio.h>
#include <mpi.h>
#include <tgmath.h>
#include <time.h>
#include <stdlib.h>
#include <unistd.h>
#include "vector2.h"

const int N = 2;
const float SIMULATION_SOFTENING_LENGTH_SQUARED = 100.0f * 100.0f;

typedef struct {
    Vector2 position;
    Vector2 acceleration;
    Vector2 velocity;
    float mass;
} Body;

Body bodies[N];
Body processed_bodies[N];

MPI_Datatype mpi_vector2_type;
MPI_Datatype mpi_body_type;

void create_mpi_vector2_type(void);
void create_mpi_body_type(void);

void generate_bodies(void);
void simulate_with_bruteforce(void);
void simulate_with_bruteforce_aux(int startIndex, int endIndex);
void mpi_simulate_with_bruteforce(void);
Vector2 calculate_newton_gravity_acceleration(Body* first_body, Body* second_body);
void integrate_bodies(void);
void integrate(Body *body);
float get_random_in_range_from_zero_to_one(void);
void print_bodies_info(void);
void print_bodies_acceleration(void);

int processes_number;
float delta_time = 0.1;
float simulation_duration = 1.0;

int main(int argc, const char** argv) {
	int rank, size, i, j;
	double start_time, elapsed_time, temp_time;
    
    MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &processes_number);
	
	start_time = MPI_Wtime();
    
    if (processes_number < 2) {
        fprintf(stderr, "Requires at least two processes.\n");
        exit(-1);
    }
    
    create_mpi_vector2_type();
    create_mpi_body_type();
    
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	
	elapsed_time = MPI_Wtime();
	if (rank == 0) {
        generate_bodies();
        
        print_bodies_info();
		
        //simulate_with_bruteforce();
        
		while (elapsed_time - start_time < simulation_duration) {
			mpi_simulate_with_bruteforce();
			for (i = 1; i < processes_number; ++i) {
				MPI_Recv(processed_bodies, N, mpi_body_type, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

				int bodies_per_rank = N / (processes_number - 1);

				int endIndex = bodies_per_rank * i;

				if (N - endIndex < bodies_per_rank) {
					endIndex = N;
				}

				for (j = bodies_per_rank * (i - 1); j < endIndex; ++j) {
					bodies[j] = processed_bodies[j];
				}
			}
			
			integrate_bodies();
			print_bodies_acceleration();
        
            temp_time = MPI_Wtime();
            while (elapsed_time - temp_time < delta_time) {
                elapsed_time = MPI_Wtime();
            }
		}
		
        MPI_Abort(MPI_COMM_WORLD, 0);
    } else {
		while (1) {
			MPI_Recv(bodies, N, mpi_body_type, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

			int bodies_per_rank = N / (processes_number - 1);

			int endIndex = bodies_per_rank * rank;

			if (N - endIndex < bodies_per_rank) {
				endIndex = N;
			}

			simulate_with_bruteforce_aux(bodies_per_rank * (rank - 1), endIndex);

			MPI_Send(&bodies, N, mpi_body_type, 0, 0, MPI_COMM_WORLD);
		}
    }
    
    MPI_Finalize();
	
	return 0;
}

void create_mpi_vector2_type(void) {
    const int fields_number = 2;
    const int blocklengths[2] = { 1, 1 };
    MPI_Datatype field_types[2] = { MPI_FLOAT, MPI_FLOAT };
    MPI_Aint offsets[2];
    
    offsets[0] = offsetof(Vector2, x);
    offsets[1] = offsetof(Vector2, y);
    
    MPI_Type_create_struct(fields_number, blocklengths, offsets, field_types, &mpi_vector2_type);
    MPI_Type_commit(&mpi_vector2_type);
}

void create_mpi_body_type(void) {
    const int fields_number = 4;
    const int blocklengths[4] = { 1, 1, 1, 1 };
    MPI_Datatype field_types[4] = {
        mpi_vector2_type,
        mpi_vector2_type,
        mpi_vector2_type,
        MPI_FLOAT
    };
    MPI_Aint offsets[4];
    
    offsets[0] = offsetof(Body, position);
    offsets[1] = offsetof(Body, acceleration);
    offsets[2] = offsetof(Body, velocity);
    offsets[3] = offsetof(Body, mass);
    
    MPI_Type_create_struct(fields_number, blocklengths, offsets, field_types, &mpi_body_type);
    MPI_Type_commit(&mpi_body_type);
}

void generate_bodies(void) {
    int i;
    
    srand(time(NULL));
    
    for (i = 0; i < N; ++i) {
        float angle = ((float) i / N) * 2.0f * M_PI +
        (get_random_in_range_from_zero_to_one() - 0.5f) * 0.5f;
        
        Body* body = &bodies[i];
        
        body->position = (Vector2) {
            .x = get_random_in_range_from_zero_to_one(),
            .y = get_random_in_range_from_zero_to_one()
        };
        
        body->velocity = (Vector2) {
            .x = cos(angle),
            .y = cos(angle)
        };
        
        float initialMass = 100000.0f;
        body->mass = initialMass * get_random_in_range_from_zero_to_one() + initialMass * 0.5f;
    }
}

void mpi_simulate_with_bruteforce(void) {
    int i;
    
    for (i = 1; i < processes_number; ++i) {
        MPI_Send(&bodies, N, mpi_body_type, i, 0, MPI_COMM_WORLD);
    }
}

void simulate_with_bruteforce(void) {
	simulate_with_bruteforce_aux(0, N);
}

void simulate_with_bruteforce_aux(int startIndex, int endIndex) {
    int i, j;
    Vector2 totalAcceleration;
    
    for (i = startIndex; i < endIndex; ++i) {
        totalAcceleration = VECTOR2_ZERO;
		
        for (j = 0; j < N; ++j) {
            if (i == j) continue;
            
            Vector2 acceleration = calculate_newton_gravity_acceleration(&bodies[i], &bodies[j]);
            vector2_perform_assignment_by_sum(&totalAcceleration, &acceleration);
        }
		
        bodies[i].acceleration = totalAcceleration;
    }
}

Vector2 calculate_newton_gravity_acceleration(Body* first_body, Body* second_body) {
    Vector2 acceleration,
            galacticPlaneR,
            galacticPlaneRTimesScale;
    float distanceSquared,
          distanceSquaredCubed,
          inverse,
          scale;
    
    acceleration = VECTOR2_ZERO;
    
    galacticPlaneR = vector2_perform_subtraction(&first_body->position, &second_body->position);
    
    distanceSquared = vector2_square_magnitude(&galacticPlaneR) + SIMULATION_SOFTENING_LENGTH_SQUARED;
    
    distanceSquaredCubed = pow(distanceSquared, 3);
    
    inverse = 1.0f / sqrt(distanceSquaredCubed);
    
    scale = second_body->mass * inverse;
    
    galacticPlaneRTimesScale = vector2_perform_multiplication(&galacticPlaneR, scale);
    
	vector2_perform_assignment_by_sum(&acceleration, &galacticPlaneRTimesScale);
    
    return acceleration;
}

void integrate_bodies(void) {
    int i;
    
    for (i = 0; i < N; ++i) {
        integrate(&bodies[i]);
    }
}

void integrate(Body *body) {
    Vector2 acceleration_times_delta_time =
        vector2_perform_multiplication(&body->acceleration, delta_time);
    vector2_perform_assignment_by_sum(&body->velocity, &acceleration_times_delta_time);
    Vector2 velocity_times_delta_time =
        vector2_perform_multiplication(&body->velocity, delta_time);
    vector2_perform_assignment_by_sum(&body->position, &velocity_times_delta_time);
}

float get_random_in_range_from_zero_to_one(void) {
    return (rand() % 100) / 100.0f;
}

void print_bodies_info(void) {
    int i;
    
    printf("%d\n", N);
	printf("%f\n", simulation_duration);
    printf("%f\n", delta_time);
    for (i = 0; i < N; ++i) {
        Body* body = &bodies[i];
        printf("%f %f\n", body->position.x, body->position.y);
        printf("%f %f\n", body->acceleration.x, body->acceleration.y);
        printf("%f %f\n", body->velocity.x, body->velocity.y);
        printf("%f\n", body->mass);
    }
}

void print_bodies_acceleration(void) {
    int i;
    
    for (i = 0; i < N; ++i) {
        Body* body = &bodies[i];
        printf("%f %f\n", body->acceleration.x, body->acceleration.y);
    }
}

