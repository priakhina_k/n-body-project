//
//  utils.h
//  N-Body Simulation
//
//  Created by Kseniia Priakhina on 12/24/17.
//  Copyright © 2017 AUCA. All rights reserved.
//

#ifndef vector2_h
#define vector2_h

#include <stdio.h>

typedef struct {
    float x, y;
} Vector2;

extern const Vector2 VECTOR2_ZERO;

Vector2 vector2_perform_addition(Vector2* first_operand, Vector2* second_operand);
Vector2 vector2_perform_subtraction(Vector2* minuend, Vector2* subtrahend);
Vector2 vector2_perform_multiplication(Vector2* vector, float factor);
void vector2_perform_assignment_by_sum(Vector2* assignee, Vector2* addend);
float vector2_square_magnitude(Vector2* vector);

#endif /* vector2_h */