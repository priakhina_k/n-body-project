//
//  utils.c
//  N-Body Simulation
//
//  Created by Kseniia Priakhina on 12/24/17.
//  Copyright © 2017 AUCA. All rights reserved.
//

#include "vector2.h"
#include <tgmath.h>

const Vector2 VECTOR2_ZERO = { .x = 0, .y = 0 };

Vector2 vector2_perform_addition(Vector2* first_operand, Vector2* second_operand) {
    return (Vector2) {
        .x = first_operand->x + second_operand->x,
        .y = first_operand->y + second_operand->y
    };
}

Vector2 vector2_perform_subtraction(Vector2* minuend, Vector2* subtrahend) {
    return (Vector2) {
        .x = minuend->x - subtrahend->x,
        .y = minuend->y - subtrahend->y
    };
}

Vector2 vector2_perform_multiplication(Vector2* vector, float factor) {
    return (Vector2) {
        .x = vector->x * factor,
        .y = vector->y * factor
    };
}

void vector2_perform_assignment_by_sum(Vector2* assignee, Vector2* addend) {
    assignee->x += addend->x;
    assignee->y += addend->y;
}

float vector2_square_magnitude(Vector2* vector) {
    return pow(sqrt(pow(vector->x, 2) + pow(vector->y, 2)), 2);
}